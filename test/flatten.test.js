const flatten = require("../flatten");
const assert = require("assert");

const testCases = [
    {value: [], expectation: []},
    {value: [1], expectation: [1]},
    {value: [[],[]], expectation: []},
    {value: [[[[[[[]]]]]]], expectation: []},
    {value: [[[[[[[1], 2], 3]]]]], expectation: [1, 2, 3]},
    {value: [[[[[[[1, 2, 3]]]]]]], expectation: [1, 2, 3]},
    {value: [[1,[[2, [[[3]]]]]]], expectation: [1, 2, 3]},
    {value: [[[[[[[]]]]], 3]], expectation: [3]},
    {value: [[[[[[[-1]]]]]]], expectation: [-1]},
    {value: [[[[[[[-1]]]]]],[[[[[[-1]]]]]],[[[[[[-1]]]]]]], expectation: [-1, -1, -1]}
];

const expectedErrors = [null, 2, "test", [2, 3, "test"], [, ,]];

const runTests = () => {
    describe("Flatten array", function () {
        it("should flatten an arbitrarily nested array", function () {
            testCases.forEach(test => {
                let flattened = flatten(test.value);
                assert.deepEqual(flattened, test.expectation);
            }); 
        });
        
        it("should throw an error when the array contains non numeric values", function () {
            expectedErrors.forEach(test => {
                assert.throws(() => flatten(test.value), Error);
            });
        });
    });   
}

runTests();