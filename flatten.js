/**
 * Flattens an arbitrarily nested array of integers (Number in Javascript). It throws an error if
 * an array of non integers is passed into the function.
 * @param   {Array<Number>} array The array of numbers to flatten
 * @returns {Array<Number}  The flattened array of numbers
 */
function flatten(array) {
    if (!Array.isArray(array)) {
        throw new Error(`flatten expects an array but got ${typeof array}`);
    }
    
    let result = [];
    array.forEach(item => {
        if (Array.isArray(item)) {
            let items = flatten(item);
            items.forEach(item => result.push(item));
        } else {
            if (typeof item !== "number") {
                throw new Error(`Expect a number but got ${typeof item}`);
            }
            result.push(item);
        }
    });
    
    return result;
}

module.exports = flatten;